##########################################################################################################################################################
##########################################################################################################################################################

include( "AthenaPoolCnvSvc/ReadAthenaPool_jobOptions.py" )

#doRetag = False 	
doRetag = True 	

from MV2defaults import default_values

JetCollections = [
  ##"AntiKt10LCTopoJets"
  # 'AntiKtVR30Rmax4Rmin02TrackJets',
  #'AntiKt2PV0TrackJets',
  #'AntiKtVR30Rmax4Rmin02TrackJets',
  #'AntiKt2PV0TrackJets',
  #'AntiKt4EMTopoJets',
  'AntiKt4HIJets',
  #'DFAntiKt4HIJets'
  #'AntiKt4EMPFlowJets',
  #'AntiKt4PV0TrackJets',
  #'AntiKt3PV0TrackJets',
  #'AntiKt4LCTopoJets',
  ]

#########################################################################################################################################################
#########################################################################################################################################################
### Define input xAOD and output ntuple file name

#import glob
#from AthenaCommon.AthenaCommonFlags import jobproperties as jp
#jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(-1)
#jp.AthenaCommonFlags.FilesInput = [
#'/afs/cern.ch/work/a/atrzupek/mc16_5TeV.420271.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1_bbfilter.recon.AOD.e7383_d1521_r11472/AOD.18626729._000023.pool.root.1',
#'/afs/cern.ch/work/a/atrzupek/mc16_5TeV.420271.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1_bbfilter.recon.AOD.e7383_d1521_r11472/AOD.18626747._000001.pool.root.1',
#'/afs/cern.ch/work/a/atrzupek/mc16_5TeV.420271.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1_bbfilter.recon.AOD.e7383_d1521_r11472/AOD.18626729._000024.pool.root.1',
#'/afs/cern.ch/work/a/atrzupek/mc16_5TeV.420271.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1_bbfilter.recon.AOD.e7383_d1521_r11472/AOD.18626729._000003.pool.root.1',
#'/eos/user/a/atrzupek/bjet/mc16_5TeV.420273.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3_bbfilter.recon.AOD.e7383_s3428_r11320/AOD.20255770._001706.pool.root.1'
#]

#set up inputs
#from glob import glob
#FDIR_JZ1 = '/eos/user/a/atrzupek/bjet/mc16_5TeV.420273.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3_bbfilter.recon.AOD.e7383_s3428_r11320'
#inFileList = glob( FDIR_JZ1+"/*.root*")
#svcMgr.EventSelector.InputCollections = inFileList

# from PyUtils import AthFile
# af = AthFile.fopen( jp.AthenaCommonFlags.FilesInput()[0] )

evtPrintoutInterval = vars().get('EVTPRINT', 5000)
svcMgr += CfgMgr.AthenaEventLoopMgr( EventPrintoutInterval=evtPrintoutInterval )

svcMgr += CfgMgr.THistSvc()

for jet in JetCollections:

  shortJetName=jet.replace("AntiKt","Akt").replace("TopoJets","To").replace("TrackJets","Tr").replace("PFlowJets","Pf")
  svcMgr.THistSvc.Output += [ shortJetName+" DATAFILE='flav_"+shortJetName+".root' OPT='RECREATE'"]

##########################################################################################################################################################
##########################################################################################################################################################

from RecExConfig.RecFlags import rec
rec.doESD.set_Value_and_Lock        (False)
rec.doWriteESD.set_Value_and_Lock   (False)
rec.doAOD.set_Value_and_Lock        (False)
rec.doWriteAOD.set_Value_and_Lock   (False)
rec.doWriteTAG.set_Value_and_Lock   (False)
rec.doDPD.set_Value_and_Lock        (False)
rec.doTruth.set_Value_and_Lock      (False)


rec.doApplyAODFix.set_Value_and_Lock(False)
include ("RecExCommon/RecExCommon_topOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

##########################################################################################################################################################
##########################################################################################################################################################
### GEO Business
# from AthenaCommon.GlobalFlags import globalflags
# print "detDescr from global flags= "+str(globalflags.DetDescrVersion)
# from AtlasGeoModel.InDetGMJobProperties import GeometryFlags as geoFlags
# print "geoFlags.Run()   = "+geoFlags.Run()
# print "geoFlags.isIBL() = "+str(  geoFlags.isIBL() )


##########################################################################################################################################################
##########################################################################################################################################################
### this is if you want to re-tag with another calibration file
from BTagging.BTaggingFlags import BTaggingFlags

#BTaggingFlags.ForceMV2CalibrationAlias = False
#BTaggingFlags.CalibrationChannelAliases += ["AntiKt2PV0Track->AntiKt2Track"]


#BTaggingFlags.OutputLevel = DEBUG

#### if the new file is already in the datatbase: simple edit the name
#BTaggingFlags.CalibrationTag = 'BTagCalibRUN12-08-42'

#### if you want to use your own calibration file use this part below
######BTaggingFlags.CalibrationFromLocalReplica = True
######BTaggingFlags.CalibrationFolderRoot = '/GLOBAL/BTagCalib/'
######BTaggingFlags.CalibrationTag = "antonello"
######BTaggingFlags.JetVertexCharge=False

include("RetagFragment.py")


##########################################################################################################################################################
### Tools

jvt = CfgMgr.JetVertexTaggerTool('JVT')
ToolSvc += jvt

ToolSvc += CfgMgr.CP__PileupReweightingTool("prw",
                                            OutputLevel = INFO,
                                            UsePeriodConfig= "MC15"
                                            )


from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator("trkIPEstimator")


# For running on xAODs without truth jets, un-comment these 2 lines:
#from DerivationFrameworkMCTruth.MCTruthCommon import addStandardTruthContents
#addStandardTruthContents()

##########################################################################################################################################################

algSeq += CfgMgr.BTagVertexAugmenter()

### Main Ntuple Dumper Algorithm
for JetCollection in JetCollections:


  shortJetName=JetCollection.replace("AntiKt","Akt").replace("TopoJets","To").replace("TrackJets","Tr").replace("PFlowJets","Pf")
  alg = CfgMgr.btagAnalysisAlg("BTagDumpAlg_"+JetCollection,
                                  OutputLevel=INFO, #DEBUG
                                  Stream=shortJetName,
                                  JVTtool=ToolSvc.JVT,
                                  )


  alg.JetCollectionName = JetCollection
  #alg.doJVT = True #if this is false JVT is NAN, if true an attempt is made to update JVT after calibration
  alg.doJVT = False #if this is false JVT is NAN, if true an attempt is made to update JVT after calibration


  alg.DefaultValDictionary = default_values
  alg.ReplaceNanDefaults = True

  if "TrackJets" in JetCollection or "Truth" in JetCollection:

    alg.CleanJets     = False
    alg.CalibrateJets = False
    alg.doJVT = False

  alg.CalibrateJets = False
  alg.JetCleaningTool.CutLevel= "LooseBad"
  alg.JetCleaningTool.DoUgly  = True



## what to include in ntuple ####
  #example
  #alg.exampleBranchInfo = False

  alg.EventInfo = True
  alg.retriveTruthJets = True
  # Flag for truth jet collection to save
  alg.TruthJetCollection = "AntiKt4TruthJets" #(default)
  #alg.TruthJetCollection = "AntiKt4TruthWZJets" # only works on AOD, for use with b-jet energy regression
  alg.JetProperties = True
  #taggers (MV2, DL1)
  alg.TaggerScores = True  #(was false )
  ##IPxD+RNNIP
  alg.ImpactParameterInfo = True  #(was false )
  ##SV1
  alg.SVInfo = True  #(was false )
  alg.svxCollections = {'jet_sv1_': 'SV1'}
  ##JetFitter
  alg.JetFitterInfo = True
  ###SoftMuonTagger
  alg.SoftMuoninfo = True
  ## b and c hadron truth info
  alg.bHadInfo = False
  alg.bHadExtraInfo = False #include all b and c decay products, and trk_origin
  #include HI  b and c decay products, and trk_origin
  alg.HIFTAGInfo = True
  #include HI b and c decay products, and trk_origin
  alg.HIFTAGExtraInfo = True

  ## kshort
  alg.kshortInfo = False

  #show debug info for branches
  alg.branchDebug = False

  #track information
  alg.TrackInfo = True
  alg.nRequiredSiHits = 2 #number of hits required to save a track
  alg.TrackCovariance = False

  #you can disable the track augmenter if youre not filling the track branches
  algSeq += CfgMgr.BTagTrackAugmenter(
    "BTagTrackAugmenter_" + JetCollection,
    OutputLevel=INFO,
  #  JetCollectionName = JetCollection,
    TrackToVertexIPEstimator = ToolSvc.trkIPEstimator,
    SaveTrackVectors = True,
   )

  alg.AccessBtagObject = True # for fatjets, turn this to False

  algSeq += alg

  ### temporarlily commented out: running on HIxAOD crash on missing xAOD::EventShape container 
  from btagAnalysis.configHelpers import get_calibration_tool
  ToolSvc += get_calibration_tool(CfgMgr, JetCollection, False)

  # from btagAnalysis.configHelpers import get_calibration_tool_2016_calib
  # ToolSvc += get_calibration_tool_2016_calib(CfgMgr, JetCollection, False)


from PerfMonComps.PerfMonFlags import jobproperties as PerfMon_jp
PerfMon_jp.PerfMonFlags.doMonitoring = False
PerfMon_jp.PerfMonFlags.doFastMon = False

###########################################################################################################################################################################
