# set up basic reader tool
import AthenaRootComps.ReadAthenaxAODHybrid

# utilities used below
from AthenaCommon.AlgSequence import AlgSequence

from btagAnalysis.configHelpers import get_short_name
from btagAnalysis.configHelpers import setupTools
from btagAnalysis.configHelpers import get_calibration_tool

# set up inputs
from glob import glob
FDIR_JZ1 = '/afs/cern.ch/work/a/atrzupek/mc16_5TeV.420271.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1_bbfilter.recon.AOD.e7383_d1521_r11472'
#FDIR_JZ1 = '/eos/user/a/atrzupek/bjet/mc16_5TeV.420273.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3_bbfilter.recon.AOD.e7383_s3428_r11320'
inFileList = glob( FDIR_JZ1+"/*.root*")
svcMgr.EventSelector.InputCollections = inFileList

#svcMgr.EventSelector.InputCollections = [
#'/eos/user/d/derendar/data_not_synced/mc16_5TeV.420267.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3R04Opt_mufilter.recon.AOD.e7190_d1521_r11472/AOD.18582063._000531.pool.root.1',
#]
#svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=100)

# define some common tools that are used by this algorithm
setupTools(ToolSvc, CfgMgr)

# start the alg sequence here
algSeq = AlgSequence()
algSeq += CfgMgr.BTagVertexAugmenter()

# set up output stream
JetCollections = [
    #'AntiKt4EMTopoJets',
    'AntiKt4HIJets',
    ]
svcMgr += CfgMgr.THistSvc()
for jet in JetCollections:
    shortJetName=get_short_name(jet)
    svcMgr.THistSvc.Output += [
        shortJetName+" DATAFILE='flav_"+shortJetName+".root' OPT='RECREATE'"]

jvt = CfgMgr.JetVertexTaggerTool('JVT')
ToolSvc += jvt

from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator("trkIPEstimator")

#doRetag = True
#from BTagging.BTaggingFlags import BTaggingFlags
#include("RetagFragment.py")


### Main Ntuple Dumper Algorithm
for JetCollection in JetCollections:
    shortJetName=get_short_name(JetCollection)
    alg = CfgMgr.btagAnalysisAlg(
        "BTagDumpAlg_"+JetCollection,
        OutputLevel=INFO,
        Stream=shortJetName,
        JVTtool=ToolSvc.JVT,
    )

    alg.JetCollectionName = JetCollection
    #if "TrackJets" in JetCollection or "Truth" in JetCollection:
        #alg.CleanJets     = False
        #alg.CalibrateJets = False
    alg.CleanJets     = False
    alg.CalibrateJets = False

    #alg.JetCleaningTool.CutLevel= "LooseBad"
    #alg.JetCleaningTool.DoUgly  = True

    ## what to include in ntuple ####
    alg.EventInfo = True
    alg.JetProperties = True
    #taggers (MV2, DL1)
    alg.TaggerScores = True
    ##IPxD+RNNIP
    alg.ImpactParameterInfo = True
    ##SV1
    alg.SVInfo = True
    alg.svxCollections = {'jet_sv1_': 'SV1'}
    ##JetFitter
    alg.JetFitterInfo = True
    ###SoftMuonTagger
    alg.SoftMuoninfo = True
    ## b and c hadron truth info
    alg.bHadInfo = False
    #include all b and c decay products, and trk_origin
    alg.bHadExtraInfo = False
    #include HI  b and c decay products, and trk_origin
    alg.HIFTAGInfo = True
    #include HI b and c decay products, and trk_origin
    alg.HIFTAGExtraInfo = True
    ## kshort
    alg.kshortInfo = False
    #example
    #alg.exampleBranchInfo = False

    #track information
    alg.TrackInfo = True
    #alg.nRequiredSiHits = 2 #number of hits required to save a track
    #alg.TrackCovariance = False

    alg.branchDebug = False

    #you can disable the track augmenter if youre not filling the track branches
    #algSeq += CfgMgr.BTagTrackAugmenter(
    # "BTagTrackAugmenter_" + JetCollection,
    # OutputLevel=INFO,
    # JetCollectionName = JetCollection,
    # TrackToVertexIPEstimator = ToolSvc.trkIPEstimator,
    # SaveTrackVectors = True,
    #)

    algSeq += alg

    ToolSvc += get_calibration_tool(CfgMgr, JetCollection, False)

