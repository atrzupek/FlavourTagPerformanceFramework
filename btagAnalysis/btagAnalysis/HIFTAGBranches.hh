#ifndef HIFTAG_BRANCHES_HH
#define HIFTAG_BRANCHES_HH

#ifndef __MAKECINT__
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/TruthParticle.h"
#endif // not __MAKECINT__


class TTree;

enum HITRKORIGIN{ HIPUFAKE=-1,
    HIFROMB,
    HIFROMC,
    HIFRAG,
    HIGEANT };

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

// branch buffers are stored as an external class to cut down on
// (re)compile time // VD I strongly diagree with it ;-)
struct HIFTAGBranchBuffer;

class HIFTAGBranches
{
public:
  // might want to add a prefix to the constructor for the tree branches
  HIFTAGBranches();
  ~HIFTAGBranches();

  // disable copying and assignment
  HIFTAGBranches& operator=(HIFTAGBranches) = delete;
  HIFTAGBranches(const HIFTAGBranches&) = delete;

  void set_tree(TTree& output_tree, bool extra_info, bool show_debug);
  void fill(const xAOD::Jet& jet, std::string jetCollectionName);
  void clear();

  inline void SetPV_x(float x){PVtx_x=x;}
  inline void SetPV_y(float y){PVtx_y=y;}
  inline void SetPV_z(float z){PVtx_z=z;}
  inline float GetPV_x(){return PVtx_x;}
  inline float GetPV_y(){return PVtx_y;}
  inline float GetPV_z(){return PVtx_z;}
  inline void SetTruePV_x(float x){truth_PV_x=x;}
  inline void SetTruePV_y(float y){truth_PV_y=y;}
  inline void SetTruePV_z(float z){truth_PV_z=z;}
  inline float GetTruePV_x(){return truth_PV_x;}
  inline float GetTruePV_y(){return truth_PV_y;}
  inline float GetTruePV_z(){return truth_PV_z;}
  float PVtx_x;
  float PVtx_y;
  float PVtx_z;
  float truth_PV_x;
  float truth_PV_y;
  float truth_PV_z;
  
private:

  bool debug;
  TTree *m_output_tree;

  HIFTAGBranchBuffer* m_branches;

  const xAOD::Jet* GetParentJet(const xAOD::Jet* Jet, std::string Keyname);

  bool GoesIntoC(const xAOD::TruthParticle* part);

  void collectChadrons(const xAOD::TruthParticle* particle,
                                          std::vector<const xAOD::IParticle*> &Chads);

  void AddMissingCHadrons(std::vector<const xAOD::IParticle*> Bhads, std::vector<const xAOD::IParticle*> &Chads);

  void GetAllChildren(const xAOD::TruthParticle* particle,
                                           std::vector<const xAOD::TruthParticle*> &tracksFromB,
                                           std::vector<const xAOD::TruthParticle*> &tracksFromC,
                                           bool isFromC);

  std::vector<int> getDRSortedIndices(std::vector<const xAOD::IParticle*> ghostHads, const xAOD::Jet *jet);

  int getTrackOrigin(const xAOD::TrackParticle *tmpTrk,
                      std::vector<const xAOD::TruthParticle*> tracksFromB,
                      std::vector<const xAOD::TruthParticle*> tracksFromC,
                      std::vector<const xAOD::TruthParticle*> tracksFromCc,
                      std::vector<const xAOD::TruthParticle*> tracksFromB1FromParent,
                      std::vector<const xAOD::TruthParticle*> tracksFromB2FromParent,
                      std::vector<const xAOD::TruthParticle*> tracksFromC1FromParent,
                      std::vector<const xAOD::TruthParticle*> tracksFromC2FromParent,
                      std::vector<const xAOD::TruthParticle*> tracksFromCNotFromB1FromParent,
                      std::vector<const xAOD::TruthParticle*> tracksFromCNotFromB2FromParent);

  bool particleInCollection( const xAOD::TrackParticle *trkPart, std::vector< ElementLink< xAOD::TrackParticleContainer > > trkColl );

};

#endif // HIFTAG_BRANCHES_HH
